const mongoose = require("mongoose");
mongoose.set("strictQuery", true);

const taskSchema = new mongoose.Schema({
  taskName: String,
  status: String,
});

module.exports = mongoose.model("Task", taskSchema);
