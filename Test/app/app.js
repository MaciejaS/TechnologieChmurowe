require("dotenv").config();
require("./database").connect();
const redis = require("redis");
const Task = require("./schema");
const redisClient = redis.createClient({ url: "redis://redis:6379" });
const express = require("express");
const app = express();
const port = process.env.PORT || 3000;
app.use(express.json());

app.get("/tasks", async (req, res) => {
  const taskArr = await Task.find({});
  res.status(200).json(taskArr);
});

app.post("/tasks", async (req, res) => {
  const task = new Task(req.body);
  task
    .save()
    .then((result) => {
      console.log(`Added task: ${result._id}`);
      res.status(201).json(result);
    })
    .catch((e) => {
      console.log(e.message);
      res.status(400).send(e.message);
    });
});

app.put("/tasks/:id", async (req, res) => {
  const id = req.params.id;
  const status = req.body.status;
  const task = await Task.findById(id);
  task.status = status;
  task.save();
  redisClient.connect();
  const count = await redisClient.incr("updatedTasks", (err, count) => {
    if (err) res.status(500).json({ error: err.message });
  });
  redisClient.quit();
  res.status(200).json({ task, updatedTasksCount: count });
});

app.listen(port, () => {
  console.log("Server working :)");
});
