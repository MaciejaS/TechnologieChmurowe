const mongoose = require("mongoose");
mongoose.set("strictQuery", true);

const user = process.env.MONGO_USERNAME;
const passwd = process.env.MONGO_PASSWORD;

exports.connect = () => {
  mongoose
    .connect(
      `mongodb://${user}:${passwd}@mongo-svc:27017/users?authSource=admin`
    )
    .then(() => {
      console.log("Successfully connected to MongoDB");
    })
    .catch((error) => {
      console.log("database connection failed. exiting now...");
      console.error(error);
      process.exit(1);
    });
};
