const express = require("express");
const app = express();
const port = 8080;
app.use(express.json());

app.get("/question", async (req, res) => {
  res.status(200).json({ answer: "This is an answer :)" });
});

app.listen(port, () => {
  console.log("Server working :)");
});
