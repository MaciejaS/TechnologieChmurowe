const express = require("express");
const axios = require("axios");
const app = express();
const port = 3000;
app.use(express.json());

app.get("/communicate", async (req, res) => {
  axios
    .get("http://microservice-b-svc:8080/question")
    .then((result) => {
      res.status(200).json({
        message: `This is an answer from microservice b: '${result.data.answer}'`,
      });
    })
    .catch((err) => {
      res.status(401).send(err.message);
    });
});

app.listen(port, () => {
  console.log("Server working :)");
});
