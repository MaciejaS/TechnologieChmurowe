const mongoose = require("mongoose");
mongoose.set("strictQuery", true);

exports.connect = () => {
  mongoose
    .connect("mongodb://mongo-svc:27017/users")
    .then(() => {
      console.log("Successfully connected to MongoDB");
    })
    .catch((error) => {
      console.log("database connection failed. exiting now...");
      console.error(error);
      process.exit(1);
    });
};
