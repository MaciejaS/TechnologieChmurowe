require("./database").connect();
const User = require("./schema");
const express = require("express");
const app = express();
const port = 8080;
app.use(express.json());
const users = require("./data.json");

async function deleteAndAdd() {
  await User.deleteMany();
  await User.insertMany(users.data);
}
deleteAndAdd();

app.get("/users", async (req, res) => {
  const userArr = await User.find({});
  res.status(200).json(userArr);
});

app.listen(port, () => {
  console.log("Server working :)");
});
