#!/bin/bash
echo "1. Create volume"
docker volume create nginx_data

echo "2. Create container"
docker run -d --name nginx-test -p 80:80 --volume nginx_data:/usr/share/nginx/html nginx

echo "3. Copy index.html"
docker cp ./index.html nginx-test:/usr/share/nginx/html/

echo "4. Curl localhost:80"
curl http://localhost:80

echo "4. Remove container"
docker rm -f nginx-test