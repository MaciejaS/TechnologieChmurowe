#!/bin/bash
docker volume create nodejs_data
docker run -d -v nodejs_data:/app --name nodejs_container node:latest
docker volume create all_volumes
docker run --rm -v all_volumes:/all_volumes -v nginx_data:/nginx_data -v nodejs_data:/nodejs_data alpine sh -c "cp -r /nginx_data /all_volumes && cp -r /nodejs_data /all_volumes"