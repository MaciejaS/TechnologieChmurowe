#!/bin/bash
echo "1. Create container"
cp $2 ./conf/nginx.conf
docker run -d --name nginx-test -p $1:80 -v ./conf:/etc/nginx/ nginx
echo ""
echo "2. Show active containers"
docker ps
echo ""
echo "3. Curl localhost:$1"
curl http://localhost:$1
echo ""
echo ""
echo "4. Remove container"
docker rm -f nginx-test