#!/bin/bash
echo "1. Create container"
docker compose up -d
echo ""
echo "2. Show active containers"
docker ps
echo ""
echo "3. Curl localhost:80"
ip=`docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' zad1-nginx-1`
echo $ip
curl http://$ip
echo ""
echo ""
echo "4. Remove container"
docker compose down