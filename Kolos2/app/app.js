require("./database").connect();
const User = require("./schema");
const express = require("express");
const app = express();
const port = process.env.PORT || 3000;
app.use(express.json());
const users = require("./data.json");

async function deleteAndAdd() {
  await User.deleteMany();
  await User.insertMany(users.data);
}
deleteAndAdd();

app.get("/users", async (req, res) => {
  const userArr = await User.find({});
  res.status(200).json(userArr);
});

app.delete("/users/:id", async (req, res) => {
  const id = req.params.id;
  await User.findByIdAndDelete(id)
    .then((result) => {
      res.status(200).send(`User ${id} deleted.`);
    })
    .catch((e) => {
      res.status(400).send(e.message);
    });
});

app.listen(port, () => {
  console.log("Server working :)");
});
